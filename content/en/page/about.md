---
author: Navdeep Kaur
title: About Me
date: 2022-01-14
description:
keywords: ["about-me", "contact"]
type: about
---

<!--more-->

<img src="https://media.giphy.com/media/hvRJCLFzcasrR4ia7z/giphy.gif" width="40" height="20" style="float: left"> 

**Hey there!**, I'm **Navdeep Kaur** 🙂, a passionate Automation Engineer. I take great care in the experience, architecture, and code quality of the things I build.

I learnt a lot from the open-source community, and I enjoy how it facilitates cooperation and information sharing.

- *I am a tools geek with a passion for automation.*
- *I have strong automation capabilities and 5+ years of work experience with cloud services such as AWS and AZURE.*

## AWARDS

- Secured State level merit position in M. Tech
- GitLab associate Certification [Certification Validation URL](https://api.badgr.io/public/assertions/EZNu-Sp2RKyryooE7KtPRA?identity__email=ernavdeepkr%40gmail.com)
- Jenkins Certification [Certification Validation URL](https://verify.skilljar.com/c/7zromr22g6q9)
- GitLab 101 Tool Certification [Certification Validation URL](https://docs.google.com/feeds/download/presentations/Export?id=1NIqy7aNXW9-lbHKANHO48ipUT1BMWkhYCkHu0Vcs900&exportFormat=pdf)
- Hackerrank Certification [Certification Validation URL](https://www.hackerrank.com/certificates/8ff7a6fc65e4)
- Course Completion Certificate for "Python Data Structures A to Z" [Certification Validation URL](http://ude.my/UC-MPHBN0JK)
- Course Completion Certificate for "Introduction To Python" [Certification Validation URL](http://ude.my/UC-90GEYNOG)
- Course Completion Certificate for "Introduction to Databases and SQL Querying"
[Certification Validation URL](http://ude.my/UC-25GAJL8I)

### SKILLS

|||||||||||
|-----|-----|----|----|----|----|----|---|---|---|
|Python|Selenium|Linux|Git|Jenkins|Ansible|Docker|AWS|MySQL|Snowflake|

### PUBLICATIONS

- A Review on how to Measure the Cloud Performance by Using Linpack benchmark in a Cloud Environment
[Certification Validation URL](https://www.ijarse.com/images/fullpdf/1404494309_3_A_REVIEW_ON_HOW_TO_MEASURE_CLOUD_PERFORMANCE_USING_LINPACK_IN_A_CLOUD_ENVIRONMENT.pdf)

- A Review on Monitoring Cloud Performance Using Linpack Benchmark on KVM in Cloudstack Platform
[Certification Validation URL](https://www.ijarse.com/images/fullpdf/1404497760_4_A_REVIEW_ON_MONITORING_CLOUD_PERFORMANCE_USING_LINPACK_BENCHMARK_ON_KVM_IN_CLOUDSTACK_PLATFORM.pdf)

- Performance Analysis of Cloud Infrastructure Using Linpack Benchmark (RESEARCH Papers)
[Certification Validation URL](https://www.ijcsmc.com/docs/papers/October2014/V3I10201412.pdf)